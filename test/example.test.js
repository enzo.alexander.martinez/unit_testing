const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("My unit tests", () => {
    before(() => {
        // Do something before testing
    });
    
    it("Adds 2 + 2 and equals 4", () => {
        expect(mylib.add(2, 2)).to.equal(4);
    });

    it("Substracts 2 to 2 and equals 0", () => {
        expect(mylib.subtract(2, 2)).to.equal(0);
    });

    it("Multiplies 2 * 3 and equals 6", () => {
        expect(mylib.multiply(2, 3)).to.equal(6);
    });

    it("Fails if divisor is 0", () => {
        // Expects it to throw an error
        // As a variable doesn't throw an error, we need to define it into a function so the expect can run the function.
        expect(() => mylib.divide(9, 0)).to.throw();
    });

    after(() => {
        // Do something after testing
    });
});