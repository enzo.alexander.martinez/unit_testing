const { AssertionError } = require("chai");

const mylib = {
    /**
     * Multiline arrow function
     * @param {Number} a 
     * @param {Number} b 
     * @returns 
     */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },

    /**
     * Multiline arrow function
     * @param {Number} a 
     * @param {Number} b 
     * @returns 
     */
    subtract: (a, b) => {
        return a - b;
    },

    /**
     * Singleline arrow function
     * @param {Number} divident 
     * @param {Number} divisor 
     * @returns 
     */
    divide: (dividend, divisor) => {
        if (divisor === 0) throw new Error('Divisor 0 not allowed');
        return dividend / divisor;
    },

    /**
     * Regular function
     * @param {Number} a 
     * @param {Number} b 
     * @returns 
     */
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;